<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Test</title>
</head>
<style>
  .background {
    display: flex;
    flex-direction: column;
    height: auto;
    width: 37rem;
    margin: 1rem 19rem;
    padding: 0rem 1rem 1rem 1rem;
    border-radius: 8px;
    border: solid 2px #4e7aa3;
  }

  .form_test {}

  .questions {
    font-weight: bold;
  }

  .answer-option {
    margin-bottom: 10px;
  }

  .btn-next {
    margin-top: 10px;
    height: 40px;
  }
</style>

<body>
  <?php
  $questions = array(
    "1" => array(
      "title" => "Đâu là một loại hình chợ tạm tự phát thường xuất hiện trong các khu dân cư?",
      "result" => "B",
      "name" => "question_1",
      "anwser" => array(
        "A" => "Ếch",
        "B" => "Cóc",
        "C" => "Nhái",
        "D" => "Thằn lằn"
      ),
    ),
    "2" => array(
      "title" => "Haiku là thể thơ truyền thống của nước nào?",
      "result" => "A",
      "name" => "question_2",
      "anwser" => array(
        "A" => "Nhật Bản",
        "B" => "Hàn Quốc",
        "C" => "Thái Lan",
        "D" => "Trung Quốc"
      ),
    ),
    "3" => array(
      "title" => "Địa danh Đắk Tô với những trận đánh nổi tiếng trong kháng chiến chống Mỹ thuộc tỉnh nào của khu vực Tây Nguyên?",
      "result" => "D",
      "name" => "question_3",
      "anwser" => array(
        "A" => " Đắk Lắk",
        "B" => "Gia Lai",
        "C" => "Đắk Nông",
        "D" => "Kon Tum"
      ),
    ),
    "4" => array(
      "title" => "Bảo tàng Hồ Chí Minh được thiết kế theo dáng một loài hoa nào?",
      "result" => "C",
      "name" => "question_4",
      "anwser" => array(
        "A" => "Hoa hướng dương",
        "B" => "Hoa hồng",
        "C" => "Hoa sen",
        "D" => "Hoa đào"
      ),
    ),
    "5" => array(
      "title" => "Màu chủ đạo của tờ tiền Polymer mệnh giá 500.000 đồng là gì?",
      "result" => "C",
      "name" => "question_5",
      "anwser" => array(
        "A" => "Đỏ",
        "B" => "Vàng",
        "C" => "Xanh",
        "D" => "Hồng"
      ),
    ),
  );
  ?>
  
  <?php
  $data = array();
  $err = false;
  $cookie_value = 0;
  $cookie_name = "total_results_page1";
  if (!empty($_POST['btnNext'])) {
    $data['question_1'] = isset($_POST['question_1']) ? $_POST['question_1'] : '';
    $data['question_2'] = isset($_POST['question_2']) ? $_POST['question_2'] : '';
    $data['question_3'] = isset($_POST['question_3']) ? $_POST['question_3'] : '';
    $data['question_4'] = isset($_POST['question_4']) ? $_POST['question_4'] : '';
    $data['question_5'] = isset($_POST['question_5']) ? $_POST['question_5'] : '';
    foreach ($data as $key => $value) {
      if (empty($value)) {
        $err = true;
      }
    }
    if ($err == true) {
      echo "<div style='color: red;'>Bạn chưa trả lời hết các câu hỏi</div>";
    } else {
      foreach ($questions as $key => $value) {
        $name = "question_" . $key;
        if ($value['result'] == $data[$name]) {
          $cookie_value = $cookie_value + 1;
        }
      }
      setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
      header("Location: ./page2.php");
    }
  }
  ?>
  <form method="POST" action="page1.php" id="form">
    <div class="background">
      <div class="form_test">
        <?php
        foreach ($questions as $key => $value) {
          echo '<p class="questions">Câu ' . $key . ': ' . $value['title'] . '<span style="color: red">*</span> </p> ';
          foreach ($value['anwser'] as $keyAns => $valueAns) {
            echo '<div class="answer-option">
              <input type="radio" id="' . $key . '' . $keyAns . '" name="' . $value['name'] . '" value="' . $keyAns . '">
              <label for="' . $key . '' . $keyAns . '">' . $valueAns . '</label>
            </div>';
          }
        }
        ?>
      </div>
      <input type="submit" value="Tiếp" class="btn-next" name="btnNext" />
    </div>
  </form>
</body>

</html>
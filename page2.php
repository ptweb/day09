<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Test</title>
</head>

<style>
  .background {
    display: flex;
    flex-direction: column;
    height: auto;
    width: 37rem;
    margin: 1rem 19rem;
    padding: 0rem 1rem 1rem 1rem;
    border-radius: 8px;
    border: solid 2px #4e7aa3;
  }

  .form_test {}

  .questions {
    font-weight: bold;
  }

  .answer-option {
    margin-bottom: 10px;
  }

  .btn-submit {
    margin-top: 10px;
    height: 40px;
  }
</style>

<body>

  <?php
  $questions = array(
    "6" => array(
      "title" => "Tượng đài Chiến thắng Điện Biên Phủ được dựng trên ngọn đồi nào?",
      "result" => "D",
      "name" => "question_6",
      "anwser" => array(
        "A" => " C1",
        "B" => "A1",
        "C" => "E1",
        "D" => "D1"
      ),
    ),
    "7" => array(
      "title" => "Đâu là tên một loại bánh nổi tiếng ở Huế?",
      "result" => "C",
      "name" => "question_7",
      "anwser" => array(
        "A" => "Sướng",
        "B" => "Vui",
        "C" => "Khoái",
        "D" => "Thích"
      ),
    ),
    "8" => array(
      "title" => "Trước khi lên làm tổng thống của Ukraina, ông Volodymyr Zelensky làm công việc gì?",
      "result" => "B",
      "name" => "question_8",
      "anwser" => array(
        "A" => "Doanh nhân",
        "B" => "Diễn viên hài",
        "C" => "Võ sĩ quyền Anh",
        "D" => "Bác sĩ phẫu thuật"
      ),
    ),
    "9" => array(
      "title" => "Đâu không phải là một tác phẩm của họa sĩ Trần Văn Cẩn?",
      "result" => "A",
      "name" => "question_9",
      "anwser" => array(
        "A" => "Đôi bạn",
        "B" => "Mẹ tôi",
        "C" => "Em Thúy",
        "D" => "Em gái tôi"
      ),
    ),
    "10" => array(
      "title" => "Nhà văn Kim Dung (Trung Quốc) nổi tiếng với thể loại văn học gì?",
      "result" => "B",
      "name" => "question_10",
      "anwser" => array(
        "A" => "Truyện trinh thám",
        "B" => "Tiểu thuyết kiếm hiệp",
        "C" => "Sử thi",
        "D" => "Thơ lãng mạn"
      ),
    ),
  );
  ?>

  <?php
  $data = array();
  $err = false;
  $cookie_value = 0;
  $cookie_name = "total_results_page2";
  if (!empty($_POST['btnSubmit'])) {
    $data['question_6'] = isset($_POST['question_6']) ? $_POST['question_6'] : '';
    $data['question_7'] = isset($_POST['question_7']) ? $_POST['question_7'] : '';
    $data['question_8'] = isset($_POST['question_8']) ? $_POST['question_8'] : '';
    $data['question_9'] = isset($_POST['question_9']) ? $_POST['question_9'] : '';
    $data['question_10'] = isset($_POST['question_10']) ? $_POST['question_10'] : '';
    foreach ($data as $key => $value) {
      if (empty($value)) {
        $err = true;
      }
    }
    if ($err == true) {
      echo "<div style='color: red;'>Bạn chưa trả lời hết các câu hỏi</div>";
    } else {
      foreach ($questions as $key => $value) {
        $name = "question_" . $key;
        if ($value['result'] == $data[$name]) {
          $cookie_value = $cookie_value + 1;
        }
      }
      setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
      header("Location: ./page3.php");
    }
  }
  ?>

  <form method="POST" action="page2.php" id="form">
    <div class="background">
      <div class="form_test">
        <?php
        foreach ($questions as $key => $value) {
          echo '<p class="questions">Câu ' . $key . ': ' . $value['title'] . '<span style="color: red">*</span> </p>';
          foreach ($value['anwser'] as $keyAns => $valueAns) {
            echo '<div class="answer-option">
              <input type="radio" id="' . $key . '' . $keyAns . '" name="' . $value['name']  . '" value="' . $keyAns . '">
              <label for="' . $key . '' . $keyAns . '">' . $valueAns . '</label>
            </div>';
          }
        }
        ?>
      </div>
      <input type="submit" value="Nộp bài" class="btn-submit" name="btnSubmit" />
    </div>
  </form>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Kết quả Test</title>
</head>
<style>
  .background {
    display: flex;
    flex-direction: column;
    height: auto;
    width: 37rem;
    margin: 1rem 19rem;
    padding: 1rem 2rem;
    border-radius: 8px;
    border: solid 2px #4e7aa3;
  }
</style>

<body>
  <div class="background">
    <?php
    if (isset($_COOKIE['total_results_page1']) && isset($_COOKIE['total_results_page2'])) {
      $result = (int)$_COOKIE['total_results_page1'] + (int)$_COOKIE['total_results_page2'];
      $a = $result * 1;
      echo "<p style='margin-top: 10px'>Chúc mừng bạn hoàn thành bài test! </p>";
      echo "<p style='margin: 0px'>Số câu đúng là:  $result </p>";
      echo "<p>Điểm: $a </p>";
      if ($result < 4) {
        echo "Bạn quá kém, cần ôn tập thêm";
      } else if ($result >= 4 && $result <= 7) {
        echo "Cũng bình thường";
      } else {
        echo "Sắp sửa làm được trợ giảng lớp PHP";
      }
    }
    ?>
  </div>
</body>

</html>